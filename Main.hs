{-# LANGUAGE BangPatterns #-}

import qualified Data.DList as DList
import qualified Data.Sequence as Seq

import Control.DeepSeq
import Data.Foldable
import Criterion.Main

newtype Tickish = Tickish Int
          deriving (Show)

instance NFData Tickish where
  rnf (Tickish n) = rnf n

data Expr = App Expr Expr
          | Tick Tickish Expr
          | Var Int
          deriving (Show)

instance NFData Expr where
  rnf (App a b) = rnf a `seq` rnf b
  rnf (Tick a b) = rnf a `seq` rnf b
  rnf (Var a) = rnf a

appChain :: Int -> Expr
appChain = go
  where
    go 0 = Var 0
    go n = go (n-1) `App` (Var n)

appTickChain :: Int -> Expr
appTickChain = go
  where
    go 0 = Var 0
    go n = Tick (Tickish n) $ go (n-1) `App` (Var n)

collectArgsTicks_Seq :: Expr -> (Expr, [Expr], Seq.Seq Tickish)
collectArgsTicks_Seq = go mempty mempty
  where
    go :: [Expr] -> Seq.Seq Tickish -> Expr -> (Expr, [Expr], Seq.Seq Tickish)
    go as ts (App f a) = go (a:as) ts f
    go as ts (Tick t e) = go as (ts <> Seq.singleton t) e
    go as ts a = (a, as, ts)

collectArgsTicks_List :: Expr -> (Expr, [Expr], [Tickish])
collectArgsTicks_List = go mempty mempty
  where
    go :: [Expr] -> [Tickish] -> Expr -> (Expr, [Expr], [Tickish])
    go as ts (App f a) = go (a:as) ts f
    go as ts (Tick t e) = go as (t:ts) e
    go as ts a = (a, as, reverse ts)

collectArgsTicks_DList :: Expr -> (Expr, [Expr], [Tickish])
collectArgsTicks_DList = go mempty mempty
  where
    go :: [Expr] -> DList.DList Tickish -> Expr -> (Expr, [Expr], [Tickish])
    go as ts (App f a) = go (a:as) ts f
    go as ts (Tick t e) = go as (ts <> DList.singleton t) e
    go as ts a = (a, as, toList ts)

collectArgsTicks_OnStack :: Expr -> (Expr, [Expr], [Tickish])
collectArgsTicks_OnStack = go mempty
  where
    go :: [Expr] -> Expr -> (Expr, [Expr], [Tickish])
    go as (App f a) = go (a:as) f
    go as (Tick t a) =
      let !(e', as', ts) = go as a
      in (e', as', t:ts)
    go as a = (a, as, [])

collectArgsTicksBenchmarks :: Expr -> [Benchmark]
collectArgsTicksBenchmarks xs =
  [ bench "sequence" $ nf (collectArgsTicks_Seq) xs
  , bench "dlist" $ nf (collectArgsTicks_DList) xs
  , bench "list" $ nf (collectArgsTicks_List) xs
  , bench "on-stack" $ nf (collectArgsTicks_OnStack) xs
  ]

collectArgs_Seq :: Expr -> (Expr, Seq.Seq Expr)
collectArgs_Seq = go mempty
  where
    go :: Seq.Seq Expr -> Expr -> (Expr, Seq.Seq Expr)
    go acc (App a b) = go (acc Seq.|> b) a
    go acc a = (a, acc)

collectArgs_List :: Expr -> (Expr, [Expr])
collectArgs_List = go mempty
  where
    go :: [Expr] -> Expr -> (Expr, [Expr])
    go acc (App a b) = go (b:acc) a
    go acc a = (a, reverse acc)

collectArgs_DList :: Expr -> (Expr, [Expr])
collectArgs_DList = go mempty
  where
    go :: DList.DList Expr -> Expr -> (Expr, [Expr])
    go acc (App a b) = go (acc <> DList.singleton b) a
    go acc a = (a, toList acc)

collectArgs_OnStack :: Expr -> (Expr, [Expr])
collectArgs_OnStack = go
  where
    go :: Expr -> (Expr, [Expr])
    go (App a b) =
      let !(e', as) = go a
      in (e', b:as)
    go a = (a, [])

collectArgsBenchmarks :: Int -> Benchmark
collectArgsBenchmarks len = lengthGroup len
  [ bench "sequence" $ nf (collectArgs_Seq) xs
  , bench "dlist" $ nf (collectArgs_DList) xs
  , bench "list" $ nf (collectArgs_List) xs
  , bench "on-stack" $ nf (collectArgs_OnStack) xs
  ]
  where
    xs = appChain len


rebuild_Seq :: [a] -> Seq.Seq a
rebuild_Seq = go mempty
  where
    go :: Seq.Seq a -> [a] -> Seq.Seq a
    go acc (x:xs) = go (acc Seq.|> x) xs
    go acc [] = acc

rebuild_List :: [a] -> [a]
rebuild_List = go mempty
  where
    go :: [a] -> [a] -> [a]
    go acc (x:xs) = go (x : acc) xs
    go acc [] = reverse acc

rebuild_DList :: [a] -> [a]
rebuild_DList = go mempty
  where
    go :: DList.DList a -> [a] -> [a]
    go acc (x:xs) = go (acc <> DList.singleton x) xs
    go acc [] = toList acc

rebuildBenchmarks :: Int -> Benchmark
rebuildBenchmarks len = lengthGroup len
  [ bench "sequence" $ whnf (length . rebuild_Seq) xs
  , bench "dlist" $ whnf (length . rebuild_DList) xs
  , bench "list" $ whnf (length . rebuild_List) xs
  ]
  where
    xs = [1..len]

lengths = [1,2,5,10,20,50,100,200]

lengthGroup :: Int -> [Benchmark] -> Benchmark
lengthGroup n = bgroup ("length=" ++ show n)

main :: IO ()
main = defaultMain
  [ bgroup "rebuild" $ map rebuildBenchmarks lengths
  , bgroup "collectArgs" $ map collectArgsBenchmarks lengths
  , bgroup "collectArgsTicks"
    [ bgroup "no-ticks"
      [ lengthGroup len $ collectArgsTicksBenchmarks $ appChain len
      | len <- lengths
      ]
    , bgroup "with-ticks"
      [ lengthGroup len $ collectArgsTicksBenchmarks $ appTickChain len
      | len <- lengths
      ]
    ]
  ]
